// JQUERY
jQuery( document ).ready(function( $ ) {

  // category tags
  $(document).on( 'click', '.blog-terms .load-more', function() {
    $(this).parent().parent().siblings('.hidden').show();
    $(this).parent().parent().siblings('.hidden').css('display', 'inline-block');
    $(this).addClass('hidden');
    $('.blog-terms .load-less').show();
  });

  // category tags
  $(document).on( 'click', '.blog-terms .load-less', function() {
    $(this).parent().parent().siblings('.hidden').hide();
    $(this).parent().parent().siblings('.hidden').css('display', 'none');
    $('.blog-terms .load-more').removeClass('hidden');
    $('.blog-terms .load-less').hide();
  });

  $(document).on('click', '.scrolltop', function(){
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    return false;
  });

  // Slick article header
  $('.article-slider-wrapper').slick({
    arrows: false,
    slidesToShow: 3,
    slide: '.column',
    infinite: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  // Slick teammembers
  $('.partner-slider-wrapper').slick({
    arrows: false,
    slidesToShow: 6,
    infinite: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }
    ]
  });

});
