<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon.ico">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <link rel="preload" href="<?php bloginfo('stylesheet_directory'); ?>/assets/fonts/fa-solid-900.woff2" as="font" crossorigin>
    <link rel="preload" href="<?php bloginfo('stylesheet_directory'); ?>/assets/fonts/fa-regular-400.woff2" as="font" crossorigin>
    <link rel="preload" href="<?php bloginfo('stylesheet_directory'); ?>/assets/fonts/fa-light-300.woff2" as="font" crossorigin>
    <link rel="preload" href="<?php bloginfo('stylesheet_directory'); ?>/assets/fonts/fa-brands-400.woff2" as="font" crossorigin>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
    <script>
      var wpurl   = '<?php bloginfo( 'wpurl' ); ?>';
      var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
      var stylesheet_directory = '<?php echo get_bloginfo( 'stylesheet_directory' ); ?>';
    </script>
  </head>
  <body <?php body_class(); ?>>
    <a href="#main" class="skiplink"><?php _e( 'To main content', 'strl' ); ?></a>
    <a href="#menu" class="skiplink"><?php _e( 'To navigation', 'strl' ); ?></a>
    <!-- header -->
    <header>
      <div class="top-nav">
        <div class="row">
          <div class="columns large-12 medium-12 small-12">
            <div class="right-menu">
              <?php
              foundationPress_menu_main( 'topnav' );
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="header-wrap">
        <div class="row">
          <div class="columns large-12 medium-12 small-12">
            <div class="inner-wrap">
              <div class="menutoggle" data-responsive-toggle="menu" data-hide-for="medium">
                <button class="menu-icon" type="button" data-toggle="menu"></button>
                <div class="title-bar-title"><?php _e( 'Menu', 'strl' );?></div>
              </div>

              <a class="logo-wrap" href="<?php bloginfo('wpurl'); ?>"><span class="screen-reader-text"><?php _e( 'Prenatal', 'strl' ); ?></span>
                <div class="logo" style="background-image: url('<?php bloginfo('stylesheet_directory') ?>/assets/img/logo-dhh.svg');"></div>
              </a>

              <nav id="menu" aria-label="Main menu">
                <?php
                foundationPress_menu_main( 'main' );
                ?>
              </nav>

              <ul class="right-menu">
                <li><a href="/zoeken">Zoeken <i class="fas fa-search"></i></a></li>
                <li><a href="/zoeken">Mijn health hub <i class="fas fa-chevron-down"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
    <section class="blue-header-space">
    </section>
    <!-- end:header -->
