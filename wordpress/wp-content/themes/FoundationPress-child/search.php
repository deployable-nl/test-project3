<?php get_header(); ?>

  <main id="main">
    <?php
    while( have_posts() ){
      the_post();
      if( have_rows( 'blocks' ) ){
        while( have_rows( 'blocks' ) ){
          the_row();
          get_template_part( 'blocks/'.get_row_layout() );
        }
      }
    }
    ?>
  </main>

<?php get_footer(); ?>