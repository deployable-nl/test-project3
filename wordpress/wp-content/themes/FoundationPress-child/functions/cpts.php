<?php
register_post_type( 'partner',
  [
    'labels'                  => [
      'name'                  => __( 'Partners', 'strl' ),
      'singular_name'         => __( 'Partner', 'strl' ),
      'menu_name'             => __( 'Partners', 'strl' ),
      'name_admin_bar'        => __( 'Partner', 'strl' ),
      'add_new'               => __( 'New partner', 'strl' ),
      'add_new_item'          => __( 'Add new partner', 'strl' ),
      'edit_item'             => __( 'Edit partner', 'strl' ),
      'new_item'              => __( 'New partner', 'strl' ),
      'view_item'             => __( 'View partner', 'strl' ),
      'search_items'          => __( 'Seach partner', 'strl' ),
      'not_found'             => __( 'Partner not found', 'strl' ),
      'not_found_in_trash'    => __( 'No partner found', 'strl' ),
      'all_items'             => __( 'All partners', 'strl' )
    ],
    'public'                  => true,
    'has_archive'             => false,
    'rewrite'                 => [ 'slug' => 'partner' ],
    'menu_position'           => 5,
    'menu_icon'               => 'dashicons-welcome-widgets-menus',
    'supports'                => [ 'title', 'thumbnail', 'author' ]
  ]
);

register_post_type( 'article',
  [
    'labels'                  => [
      'name'                  => __( 'Artikelen', 'strl' ),
      'singular_name'         => __( 'Artikel', 'strl' ),
      'menu_name'             => __( 'Artikelen', 'strl' ),
      'name_admin_bar'        => __( 'Artikelen', 'strl' ),
      'add_new'               => __( 'New Artikel', 'strl' ),
      'add_new_item'          => __( 'Add new Artikel', 'strl' ),
      'edit_item'             => __( 'Edit Artikel', 'strl' ),
      'new_item'              => __( 'New Artikel', 'strl' ),
      'view_item'             => __( 'View Artikel', 'strl' ),
      'search_items'          => __( 'Seach Artikel', 'strl' ),
      'not_found'             => __( 'Artikel not found', 'strl' ),
      'not_found_in_trash'    => __( 'No Artikel found', 'strl' ),
      'all_items'             => __( 'All Artikelen', 'strl' )
    ],
    'public'                  => true,
    'has_archive'             => false,
    'rewrite'                 => [ 'slug' => 'artikel' ],
    'menu_position'           => 5,
    'menu_icon'               => 'dashicons-welcome-widgets-menus',
    'supports'                => [ 'title', 'editor', 'thumbnail', 'author', 'excerpt' ]
  ]
);

register_post_type( 'events',
  [
    'labels'                  => [
      'name'                  => __( 'Events', 'strl' ),
      'singular_name'         => __( 'Event', 'strl' ),
      'menu_name'             => __( 'Events', 'strl' ),
      'name_admin_bar'        => __( 'Events', 'strl' ),
      'add_new'               => __( 'New Event', 'strl' ),
      'add_new_item'          => __( 'Add new Event', 'strl' ),
      'edit_item'             => __( 'Edit Event', 'strl' ),
      'new_item'              => __( 'New Event', 'strl' ),
      'view_item'             => __( 'View Event', 'strl' ),
      'search_items'          => __( 'Seach Event', 'strl' ),
      'not_found'             => __( 'Event not found', 'strl' ),
      'not_found_in_trash'    => __( 'No Event found', 'strl' ),
      'all_items'             => __( 'All Events', 'strl' )
    ],
    'public'                  => true,
    'has_archive'             => false,
    'rewrite'                 => [ 'slug' => 'event' ],
    'menu_position'           => 5,
    'menu_icon'               => 'dashicons-welcome-widgets-menus',
    'supports'                => [ 'title', 'thumbnail', 'author' ]
  ]
);


add_action( 'init', 'create_taxonomies' );
function create_taxonomies(){
	register_taxonomy(
		'article_category',
		'article',
		[
			'label'              => __( 'Categorieën', 'strl' ),
			'rewrite'            => [ 'slug' => 'categorie' ],
			'hierarchical'       => true,
      'show_admin_column'  => true
    ]
	);

  register_taxonomy(
    'tag',
    'article',
    [
     'hierarchical' => false,
     'labels' => $labels,
     'show_ui' => true,
     'update_count_callback' => '_update_post_term_count',
     'query_var' => true,
     'rewrite' => array( 'slug' => 'tag' ),
    ]
  );

}

?>
