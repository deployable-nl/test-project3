<?php
add_action( 'admin_enqueue_scripts', 'strl_acf_enqueue' );
function strl_acf_enqueue( $hook ){
  wp_enqueue_script( 'my_custom_script', get_bloginfo( 'stylesheet_directory' ) . '/assets/js/vendor.min.js' );
}

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_image_size( 'featured-xlarge', 1920, 1080, true );

remove_image_size( 'featured-small' );
remove_image_size( 'featured-medium' );
remove_image_size( 'featured-large' );

// register_sidebar( array(
//   'id'            => 'vacature',
//   'name'          => __( 'Vacature widgets', 'foundationpress' ),
//   'description'   => __( 'Drag widgets to this sidebar container.', 'foundationpress' ),
//   'before_widget' => '<article id="%1$s" class="widget %2$s">',
//   'after_widget'  => '</article>',
//   'before_title'  => '<h6>',
//   'after_title'   => '</h6>',
// ));

add_filter( 'image_size_names_choose', 'custom_image_size' );
function custom_image_size( $sizes ) {
  return array_merge( $sizes, [
    'featured-xlarge' => __( 'featured xLarge (1920 - 1080)', 'strl' ),
  ] );
}

register_nav_menus( [
  'main' => 'Main menu',
] );

if ( !function_exists( 'foundationPress_menu_main' ) ) {
	function foundationPress_menu_main( $menuname ) {
    wp_nav_menu( [
      'container'      => false,
      'menu'           => $menuname,
      'menu_class'     => 'vertical large-horizontal menu',
      'theme_location' => 'primary',
      'items_wrap'     => '<ul id="%1$s" class="%2$s" data-responsive-menu="drilldown large-dropdown" data-parent-link="true" data-back-button=\'<li class="js-drilldown-back"><a tabindex="0">Terug</a></li>\'>%3$s</ul>',
      'fallback_cb'    => false,
      'walker'         => new F6_DRILL_MENU_WALKER()
    ] );
	}
}

class F6_DRILL_MENU_WALKER extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = [] ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul class=\"vertical menu\">\n";
	}
}

// PAGINATION
if( !function_exists( 'pagination' ) ){
  function pagination(){
  	global $wp_query;

  	$big = 999999999;

  	$paginate_links = paginate_links( [
  		'base'        => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
  		'current'     => max( 1, get_query_var( 'paged' ) ),
  		'total'       => $wp_query->max_num_pages,
  		'mid_size'    => 5,
  		'prev_next'   => True,
  	    'prev_text' => __( '&laquo;', '' ),
  	    'next_text' => __( '&raquo;', '' ),
  		'type'        => 'list'
  	] );

    $paginate_links = str_replace( "<ul class='page-numbers'>", "<ul class='pagination'>", $paginate_links );

  	// Display the pagination if more than one page is found
  	if( $paginate_links ){
  		echo '<div class="pagination-centered">';
  		echo $paginate_links;
  		echo '</div><!--// end .pagination -->';
  	}
  }
}

/**
 * A fallback when no navigation is selected by default.
 */

if( !function_exists( 'menu_fallback' ) ){
  function menu_fallback(){
  	echo '<div class="alert-box secondary">';
  	// Translators 1: Link to Menus, 2: Link to Customize
  	printf( __( 'Please assign a menu to the primary menu location under %1$s or %2$s the design.', '' ),
  		sprintf(  __( '<a href="%s">Menus</a>', '' ),
  			get_admin_url( get_current_blog_id(), 'nav-menus.php' )
  		),
  		sprintf(  __( '<a href="%s">Customize</a>', '' ),
  			get_admin_url( get_current_blog_id(), 'customize.php' )
  		)
  	);
  	echo '</div>';
  }
}

// Add Foundation 'active' class for the current menu item
if( !function_exists( 'active_nav_class' ) ){
  function active_nav_class( $classes, $item ) {
    if ( $item->current == 1 || $item->current_item_ancestor == true ) {
      $classes[] = 'active';
    }
    return $classes;
  }
  add_filter( 'nav_menu_css_class', 'active_nav_class', 10, 2 );
}

/**
 * Use the active class of ZURB Foundation on wp_list_pages output.
 * From required+ Foundation http://themes.required.ch
 */
if( !function_exists( 'active_list_pages_class' ) ){
function active_list_pages_class( $input ) {
  $pattern = '/current_page_item/';
  $replace = 'current_page_item active';

  $output = preg_replace( $pattern, $replace, $input );

  return $output;
  }
  add_filter( 'wp_list_pages', 'active_list_pages_class', 10, 2 );
}

if( !get_site_option( 'strl_installed' ) ){
  // REMOVE DEFAULT COMMENT
  wp_delete_comment( 1 );

  // DELETE FIRST POST
  global $wpdb;
  $statement    = "DELETE FROM $wpdb->posts WHERE id = 1";
  $results      = $wpdb->query( $statement );
  $statement    = "DELETE FROM $wpdb->posts WHERE id = 3";
  $results      = $wpdb->query( $statement );
  $statement    = "DELETE FROM $wpdb->posts WHERE id = 2";
  $results      = $wpdb->query( $statement );

  // UPDATE options
  update_site_option( 'timezone_string', 'Europe/Amsterdam' );
  update_site_option( 'WPLANG', 'nl_NL' );
  update_site_option( 'default_comment_status', 'closed' );
  update_site_option( 'blogdescription', '' );
  update_site_option( 'permalink_structure', '/%postname%/' );

  update_site_option( 'wpv_options', [ 'wpv_codemirror_autoresize' => 1 ] );
  wp_cache_delete( 'alloptions', 'options' );

  $toolset_options = unserialize( 'a:4:{s:25:"toolset_bootstrap_version";s:2:"-1";s:23:"types-information-table";a:3:{s:12:"show-on-post";s:3:"off";s:17:"show-on-post-type";s:3:"off";s:19:"show-on-field-group";s:3:"off";}s:20:"shortcodes_generator";s:7:"disable";s:23:"show_admin_bar_shortcut";s:3:"off";}' );

  update_option( 'toolset_options', $toolset_options );

  wp_cache_delete ( 'alloptions', 'options' );

  $title            = 'Kitchensink';
  $content          = 'Dit is de Kitchensink.';
  $check            = get_page_by_title( $title );
  $kitchensink      = [
    'post_type'     => 'page',
    'post_title'    => $title,
    'post_content'  => $content,
    'post_status'   => 'publish',
    'post_author'   => 1,
    'post_slug'     => 'kitchensink'
  ];

  if( !isset( $check->ID ) ){
    wp_insert_post( $kitchensink );
  }

  $title            = '404 Pagina niet gevonden';
  $check            = get_page_by_title( $title );
  $page404          = [
    'post_type'     => 'page',
    'post_title'    => $title,
    'post_content'  => $content,
    'post_status'   => 'publish',
    'post_author'   => 1,
    'post_slug'     => '404-pagina-niet-gevonden'
  ];

  if( !isset( $check->ID ) ){
    $page404id = wp_insert_post( $page404 );
  }

  add_filter( 'acf/load_value/name=blocks', 'strl_add_starting_repeater', 10, 3 );
  function strl_add_starting_repeater( $value, $post_id, $field ){
    $content = '<h2>Dit is een gestylede h2</h2><p>Dit is een paragraaf.</p><h3>Dit is een gestylede h3</h3><p>Dit is de tekst onder de gestylede h3.</p><h4>Dit is een gestylede h4</h4><p>Dit is de tekst onder de gestylede h4.</p><p>Pellentesque id est finibus, vulputate dolor nec, eleifend nisl. Fusce auctor cursus nisi, <a href="">ac vehicula nunc porttitor</a> vel. Integer non ante bibendum, cursus diam ut, porttitor est. Morbi et orci turpis. Cras fermentum nulla placerat, sollicitudin metus ut, vehicula metus. Quisque feugiat nibh eget risus hendrerit interdum. Vivamus vitae lobortis est. Donec eu dapibus enim. In hac habitasse platea dictumst. Morbi sit amet purus eleifend, convallis neque vitae, sodales ante. Mauris vel gravida risus. Sed vitae turpis augue.</p><ul><li>Mauris vel gravida risus.</li><li><a href="">Pellentesque id est finibus</a>.</li><li>Vulputate dolor nec.</li></ul><p>Pellentesque id est finibus, vulputate dolor nec, eleifend nisl. Fusce auctor cursus nisi, ac vehicula nunc porttitor vel.</p><ul><li>Mauris vel gravida risus.</li><li><a href="">Pellentesque id est finibus.</a></li><li>Vulputate dolor nec.</li></ul><p>Pellentesque id est finibus, vulputate dolor nec, eleifend nisl. Fusce auctor cursus nisi, ac vehicula nunc porttitor vel. </p>';

    $kitchensinkid = get_page_by_title( 'Kitchensink' )->ID;
    if( $post_id == $kitchensinkid && $value == '' ){
      $value = [
        [
          'acf_fc_layout'   => 'fullwidth',
          'fullwidth-title' => 'Kitchensink title',
          'fullwidth-text'  => $content,
        ],
      ];
    }

    $page404 = get_page_by_title( '404 Pagina niet gevonden' )->ID;
    if( $post_id == $page404 && $value == '' ){
      $value = [
        [
          'acf_fc_layout'   => 'fullwidth',
          'fullwidth-title' => '404 Pagina niet gevonden',
          'fullwidth-text'  => 'Helaas, deze pagina bestaat niet meer.',
        ],
      ];
    }
    return $value;
  }
  update_site_option( 'strl_installed', true );
}

// Add GFORM value 'page_title' for dynamic field value
add_filter( 'gform_field_value_page_title', 'strl_populate_gform_page_title' );
function strl_populate_gform_page_title( $value ){
  return get_the_ID() . ' - '  .get_the_title();
}


// Disable auto updates of Gravity Forms
// https://docs.gravityforms.com/gform_disable_auto_update/
add_filter( 'gform_disable_auto_update', '__return_true' );