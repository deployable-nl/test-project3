
	<!-- footer -->
	<footer>
		<div class="row widgets">
			<div class="columns large-3 small-12">
				<?php dynamic_sidebar( 'footer-1' ); ?>
				<a class="logo-wrap" href="<?php bloginfo('wpurl'); ?>"><span class="screen-reader-text"><?php _e( 'Prenatal', 'strl' ); ?></span>
					<div class="logo" style="background-image: url('<?php bloginfo('stylesheet_directory') ?>/assets/img/logo-dhh.svg');"></div>
				</a>
			</div>
			<div class="columns large-3 small-12">
				<?php dynamic_sidebar( 'footer-2' ); ?>
			</div>
			<div class="columns large-3 small-12">
				<?php dynamic_sidebar( 'footer-3' ); ?>
			</div>
			<div class="columns large-3 small-12">
				<?php dynamic_sidebar( 'footer-4' ); ?>
				<div class="socials">
					<h6>Socials</h6>
					<ul>
						<li><a target="_blank" href="#"><i class="fab fa-linkedin-in"></i> LinkedIn</a></li>
						<li><a target="_blank" href="#"><i class="fab fa-twitter"></i> Twitter</a></li>
						<li><a target="_blank" href="#"><i class="fab fa-youtube"></i> Youtube</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="copyright">
			<div class="row">
				<div class="columns large-12 small-12">
					<?php dynamic_sidebar( 'copyright' ); ?>
					<div class="jaarbeurs">
						<span>Dutch Health Hub is een idee van</span> <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/logo-jaarbeurs.jpg"/>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- end:footer -->
	<?php wp_footer(); ?>
</body>
</html>
