<?php get_header(); ?>

  <main id="main">
    <?php
    $args = [
      'post_type'     => 'page',
      'post__in'      => [ 7 ],
      'no_found_rows' => true
    ];
    query_posts( $args );

    while( have_posts() ){
      the_post();
      if( have_rows( 'blocks' ) ){
        while( have_rows( 'blocks' ) ){
          the_row();
          get_template_part( 'blocks'.get_row_layout() );
        }
      }
    }
    ?>
  </main>

<?php get_footer(); ?>