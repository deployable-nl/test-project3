<?php
/*
CUSTOM FUNCTIONS ALWAYS PREFIX WITH `strl_`
*/

add_shortcode( 'btn', 'strl_button_primary_shortcode' );
function strl_button_primary_shortcode( $atts, $content ){

  extract(shortcode_atts( [
    'link' 	  => '',
    'color'   => '',
		'target'  => '',
  ], $atts));

  ob_start();
  ?>
  <a href="<?php echo $link; ?>" class="btn <?php echo $color ?>" target="<?php echo $target; ?>"><?php echo $content; ?></a>
  <?php
  return ob_get_clean();
}

add_filter( 'use_block_editor_for_post', '__return_false', 10 );
add_filter( 'use_block_editor_for_post_type', '__return_false', 10 );

load_child_theme_textdomain( 'strl', get_stylesheet_directory() . '/languages' );

add_filter( 'acf/settings/save_json', 'strl_acf_json_save_point' );
function strl_acf_json_save_point( $path ){
	$path = get_stylesheet_directory().'/functions/acf-json/';
	return $path;
}

add_filter( 'acf/settings/load_json', 'strl_acf_json_load_point' );
function strl_acf_json_load_point( $paths ){
  unset($paths[0]);
  $paths[] = get_stylesheet_directory().'/functions/acf-json/';
  return $paths;
}

add_action( 'admin_head', 'strl_svg_fix' );
function strl_svg_fix(){
  echo '<style>
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {
      width: 100% !important;
      height: auto !important;
    }
  </style>';
}

add_filter( 'upload_mimes', 'strl_allow_svg' );
function strl_allow_svg($mimes){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_action( 'wp_trash_post', 'strl_restrict_post_deletion', 10, 1 );
add_action( 'before_delete_post', 'strl_restrict_post_deletion', 10, 1 );
function strl_restrict_post_deletion( $post_ID ){
  if( $post_ID === 11 ){
    echo "You are not authorized to delete this page.";
    exit;
  }
}

add_filter( 'style_loader_src', 'strl_remove_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'strl_remove_ver_css_js', 9999 );
function strl_remove_ver_css_js( $src ){
	if ( strpos( $src, 'ver=' ) ) {
    $src = remove_query_arg( 'ver', $src );
  }
	return $src;
}

// REGISTER BUTTONS
add_action( 'init', 'strl_buttons' );
function strl_buttons(){
  add_filter( 'mce_external_plugins', 'strl_add_buttons' );
  add_filter( 'mce_buttons', 'strl_register_buttons' );
}

function strl_add_buttons( $plugin_array ){
  $plugin_array['strl_buttons'] = get_bloginfo( 'stylesheet_directory' ).'/assets/js/plugin.min.js';
  return $plugin_array;
}

function strl_register_buttons( $buttons ){
  array_push( $buttons, 'buttons' );
  return $buttons;
}

function remove_dashboard_widgets(){
  global $wp_meta_boxes;

  unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
  unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
  unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
  update_user_meta( get_current_user_id(), 'show_welcome_panel', false );
  remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
  remove_meta_box( 'rg_forms_dashboard', 'dashboard', 'side' );
}
add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );

/*** DEFAULT FUNCTIONS ********************************************************************************************************************************** */

# ENQUEUE SCRIPTS & STYLES
function enqueue_scripts(){

	//wp_deregister_script( 'jquery' );

  # SCRIPTS
  wp_enqueue_script( 'vendor', get_bloginfo('stylesheet_directory').'/assets/js/vendor.min.js', [], '', true );
  wp_enqueue_script( 'scripts', get_bloginfo('stylesheet_directory').'/assets/js/scripts.min.js', [], '', true );

  # STYLES
  $style = glob( ABSPATH.'/wp-content/themes/FoundationPress-child/assets/css/style-*' );
  $style = basename( $style[count( $style )-1] );
  wp_enqueue_style( 'child-theme', get_bloginfo('stylesheet_directory').'/assets/css/'.$style );
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts', 999 );

# CLEANUP
function clean_header(){
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}
add_action( 'init', 'clean_header' );

/* Add ID field to every CPT */
$custom_post_types = get_post_types( [ 'public' => true ], 'names' );
foreach( $custom_post_types as $post_type ){
	add_action( 'manage_'.$post_type.'s_columns', 'revealid_add_id_column', 5 );
  add_filter( 'manage_'.$post_type.'s_custom_column', 'revealid_id_column_content', 5, 2 );
}

function revealid_add_id_column( $columns ){
  $checkbox = array_slice( $columns , 0, 1 );
	$columns = array_slice( $columns , 1 );

	$id['revealid_id'] = 'ID';

	$columns = array_merge( $checkbox, $id, $columns );
	return $columns;
}

function revealid_id_column_content( $column, $id ){
  if( 'revealid_id' == $column ){
    echo $id;
  }
}

add_action( 'admin_head', 'my_column_width' );
function my_column_width(){
  echo '<style type="text/css">';
  echo '.revealid_id, #revealid_id { text-align: center; width:60px !important; }';
  echo '.acf-flexible-content .layout { border: 2px solid rgba(0, 44, 63, 0.4); margin: 2px 0 0; } ';
  echo 'button.collapse { border: 2px solid rgba(0, 44, 63, 0.4); padding: .2rem .5rem; margin: -.5rem 0 1rem; float: right; outline:none; }';
	echo '.acf-spec-row { padding:.25rem .5rem; display: inline-block !important;font-size: .925rem;margin-left: 1rem;padding-right: .5rem;}';
	echo '.acf-spec-row span {display: inline-block;margin-right: .5rem;}';
  echo '.acf-spec-row:first-of-type {margin-left: 0;} ';
  echo '.acf-spec-row.dark { margin-left: 0; font-style: italic; color: #999;}';
  echo '#TB_ImageOff img { width: calc( 100% - 30px ); height: auto; margin: 30px 0 0 15px!important;}';
  echo '#TB_closeWindowButton { top: 0; }';
  echo '</style>';
}

# FUNCTIONS
require( 'functions/foundation.php' );
require( 'functions/cpts.php' );
require( 'functions/settings.php' );

// SOCIAL SHARE
add_shortcode( 'socialshare', 'social_share' );
function social_share( $atts ){

  $atts = shortcode_atts( [
    'facebook'  => 1, // 1 defaul to true
    'facebookmsg'  => 1, // 1 defaul to true
    'twitter'   => 1,
    'linkedin'  => 1,
    'mail'      => 1,
    'whatsapp'  => 1,
    'pinterest' => 1
  ], $atts );

  foreach( $atts as $key => $value ){
    if( isset( $value ) ){
      $$key = filter_var( $value, FILTER_VALIDATE_BOOLEAN );
    }
  }

  ob_start();
  ?>
  <div class="socialshare">
    <?php
    if( $facebook ){
      ?>
      <a aria-label="Delen via Facebook" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>" onClick="__gaTracker('send', 'social', 'facebook', 'share', '<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>');" target="_blank"><i data-tooltip title="Delen via Facebook" data-position="top" data-alignment="center" class="fab fa-facebook-f"></i></a>
      <?php
    }
    if( $facebookmsg ){
      ?>
      <a aria-label="Delen via Facebook Messenger" class="show-for-small-only" href="fb-messenger://share/?link=<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>?send-dialog&app_id=XXXXXXXXXX"><i data-tooltip title="Delen via Facebook Messenger" data-position="top" data-alignment="center" class="fab fa-facebook-messenger"></i></a>
      <?php
    }
    if( $twitter ){
      ?>
      <a aria-label="Delen via Twitter" href="http://twitter.com/intent/tweet?url=<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>" onClick="__gaTracker('send', 'social', 'twitter', 'share', '<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>');" target="_blank"><i data-tooltip title="Delen via Twitter" data-position="top" data-alignment="center" class="fab fa-twitter"></i></a>
      <?php
    }
    if( $linkedin ){
      ?>
      <a aria-label="Delen via LinkedIn" href="http://www.linkedin.com/shareArticle?url=<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>" onClick="__gaTracker('send', 'social', 'linkedin', 'share', '<?php echo(($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>');" target="_blank"><i data-tooltip title="Delen via LinkedIn" data-position="top" data-alignment="center" class="fab fa-linkedin-in"></i></a>
      <?php
    }
    if( $pinterest ){
      ?>
      <a aria-label="Delen via Pinterest" href="http://www.pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php if(function_exists('the_post_thumbnail')) echo wp_get_attachment_url(get_post_thumbnail_id()); ?>&description=<?php echo get_the_title(); ?> – <?php echo get_permalink(); ?>" id="pinterest" target="_blank" onClick="__gaTracker('send', 'social', 'pinterest', 'share', '<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>');" ><i data-tooltip title="Delen via Pinterest" data-position="top" data-alignment="center" class="fab fa-pinterest"></i></a>
      <?php
    }
    if( $mail ){
      ?>
      <a aria-label="Delen via Email" href="mailto:?subject=Kijk%20deze%20website!&body=<?php the_permalink(); ?>" onClick="__gaTracker('send', 'social', 'email-share', 'share', '<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>');" ><i data-tooltip title="Delen via Mail" data-position="top" data-alignment="center" class="far fa-envelope"></i></a>
      <?php
    }
    if( $whatsapp ){ ?>
      <a aria-label="Delen via Whatsapp" class="show-for-small-only" href="https://wa.me/?text=<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>" data-action="share/whatsapp/share" onClick="__gaTracker('send', 'social', 'whatsapp', 'share', '<?php echo (($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>');" ><i data-tooltip title="Delen via WhatsApp" data-position="top" data-alignment="center" class="fab fa-whatsapp"></i></a>
      <?php
    }
    ?>
  </div>
  <?php
  return ob_get_clean();
}

add_action( 'wp_loaded', 'strl_output_buffer_start' );
function strl_output_buffer_start(){
  ob_start( 'strl_output_callback' );
}

function strl_output_callback( $buffer ){
	$buffer = preg_replace( "%[ ]type=[\'\"]text\/(javascript|css)[\'\"]%", '', $buffer );
  $buffer = preg_replace( "%[ ]id=\"x-mark-icon\"%", '', $buffer );
  $buffer = preg_replace( "%[ ]id=\"control-panel-4-icon\"%", '', $buffer );
  $buffer = preg_replace( "%Dit iframe bevat de vereiste logica om Ajax aangedreven Gravity Forms te verwerken.%", '', $buffer );
	$buffer = preg_replace( "%https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js%", 'https://code.jquery.com/jquery-3.5.1.min.js', $buffer );
	$buffer = preg_replace( '%aria-multiselectable="false"%', '', $buffer );
  return $buffer;
}

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

add_action( 'admin_menu', 'strl_remove_menus' );
function strl_remove_menus(){
  remove_menu_page( 'edit.php' );
  remove_menu_page( 'edit-comments.php' );
}
