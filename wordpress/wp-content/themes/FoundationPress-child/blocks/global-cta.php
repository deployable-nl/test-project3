<?php
$rowlayout             = basename( __FILE__, ".php" );
$prefix                = $rowlayout.'-';

$titledefault          = get_field( 'cta-default-title', 'options' );
$contentdefault        = get_field( 'cta-default-content', 'options' );
$contentrightdefault   = get_field( 'cta-default-content-right', 'options' );
?>
<!-- <?php echo $prefix; ?> -->
<div class="section <?php echo $rowlayout; ?>">
  <div class="row">
    <div class="column large-5 large-offset-1 medium-12 small-12">
      <?php
        if( $titledefault ){ ?>
        <div class="cta-title">
          <?php echo $titledefault; ?>
        </div>
        <?php
      }
      if( $contentdefault ){ ?>
        <div class="text">
          <?php echo $contentdefault; ?>
        </div>
        <?php
      }
      ?>
    </div>
    <div class="column large-5 large-offset-1 medium-12 small-12">
      <?php
      if( $contentrightdefault ){
        echo $contentrightdefault;
      }
      ?>
    </div>
  </div>
</div>
<!-- end:<?php echo $prefix; ?> -->
