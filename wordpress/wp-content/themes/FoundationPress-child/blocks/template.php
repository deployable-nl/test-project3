<?php
$rowlayout        = basename( __FILE__, ".php" );
$prefix           = $rowlayout.'-';

$title            = get_sub_field( $prefix.'title' );
?>
<!-- <?php echo $prefix; ?> -->
<div class="section <?php echo $rowlayout; ?>">
  <div class="row">
    <div class="column large-12 medium-12 small-12">
      <?php
      if( $title ){
        echo $title;
      }
      ?>
    </div>
  </div>
</div>
<!-- end:<?php echo $prefix; ?> -->