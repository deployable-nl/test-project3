<?php
$rowlayout        = basename( __FILE__, ".php" );
$prefix           = $rowlayout.'-';

$title            = get_sub_field( $prefix.'title' );
$articles         = get_sub_field( $prefix.'articles-select' );
?>
<!-- <?php echo $prefix; ?> -->
<div class="section <?php echo $rowlayout; ?>">
  <div class="row">
    <?php
    if( $title ){
      ?>
      <h2><?php echo $title; ?></h2>
      <?php
    }
    ?>
    <div class="article-slider-wrapper">
      <?php
      if( $articles ){
        ?>
        <?php
        foreach( $articles as $post ){
          setup_postdata($post);
          get_template_part( 'blocks/partials/part', 'header-article' );
        } ?>
        <?php
        wp_reset_postdata();
      } ?>
    </div>
  </div>
</div>
<!-- end:<?php echo $prefix; ?> -->
