<?php
$rowlayout        = basename( __FILE__, ".php" );
$prefix           = $rowlayout.'-';

$title            = get_sub_field( $prefix.'title' );
$terms = get_terms([
    'taxonomy' => 'article_category',
    'hide_empty' => false,
]);
?>
<!-- <?php echo $prefix; ?> -->
<div class="section <?php echo $rowlayout; ?>">
  <div class="row">
    <div class="column large-3 medium-12 small-12">
      <h2>Browse door  thema’s</h2>
    </div>
    <div class="column large-8 medium-12 small-12">
      <div class="blog-terms">
        <ul>
          <?php
          $i = 1;
          foreach( $terms as $term ) {
            $name         = $term->name;
            $termlink     = get_term_link($term); ?>
            <li class="<?php echo $i > 5 ? 'hidden' : ''; ?>">
              <a target="_blank" class="link" href="<?php echo $termlink; ?>"> <?php echo $name; ?></a>
            </li>
            <?php
            $i++;
          }
          ?>
          <?php if ( $i > 5 || !$i ===  0 ) { ?>
            <li>
              <div class="btn-container"><span class="load-more">Toon meer</span></div>
              <div class="btn-container"><span class="load-less">Toon minder </span></div>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- end:<?php echo $prefix; ?> -->
