<?php
$featured_img_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
$title            = get_the_title();
$linkpost         = get_the_permalink();
$terms            = get_the_terms( get_the_ID(), 'article_category' );
?>
<div class="column column-block large-12 medium-12 small-12">
  <div class="single-article-list">
    <div class="content">
      <div class="article-category">
        <?php
        foreach( $terms as $term ){
          echo $term->name;
        }
        ?>
      </div>
      <div class="article-title"><?php echo $title; ?><i class="far fa-long-arrow-alt-right"></i></div>
      <a class="link" href="<?php echo $linkpost; ?>"><span class="screen-reader-text"><?php _e( 'Read more', 'strl' ); ?></span></a>
    </div>
    <div class="image-wrapper">
      <div class="image-container" style="background-image: url('<?php echo $featured_img_url; ?>');"></div>
    </div>
  </div>
</div>
