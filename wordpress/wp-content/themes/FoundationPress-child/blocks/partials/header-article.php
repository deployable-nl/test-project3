<?php
$featured_img_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
$title            = get_the_title();
$excerpt          = get_the_excerpt();
$linkpost         = get_the_permalink();
$date             = get_the_date( 'j F Y' );
$readingtime      = get_field( 'news-readingtime' );
$queried_object   = get_queried_object();
$terms            = get_the_terms( get_the_ID(), 'article_category' );
?>
<div class="column column-block large-4 medium-12 small-12">
  <div class="single-article">
    <div class="image-label">
      <?php
      foreach( $terms as $term ){
        echo $term->name;
      }
      ?>
    </div>
    <div class="image-wrapper">
      <div class="image-container" style="background-image: url('<?php echo $featured_img_url; ?>');"></div>
    </div>
    <div class="content">
      <h3><?php echo $title; ?></h3>
      <div class="meta-data">
        <span class="reading-time"><i class="far fa-clock"></i> <?php echo $readingtime; ?></span>
        <span class="date"><i class="far fa-calendar-alt"></i> <?php echo $date; ?></span>
      </div>
      <p><?php echo $excerpt; ?></p>
      <a class="link" href="<?php echo $linkpost; ?>"><span class="screen-reader-text"><?php _e( 'Read more', 'strl' ); ?></span></a>
    </div>
  </div>
</div>
