<?php
$rowlayout        = basename( __FILE__, ".php" );
$prefix           = $rowlayout.'-';

$article          = get_sub_field( $prefix.'article-select' );
?>
<!-- <?php echo $prefix; ?> -->
<div class="section <?php echo $rowlayout; ?>">
  <div class="row">
    <?php
    if( $article ){
      foreach( $article as $post ){
        setup_postdata($post);
        $excerpt = get_the_excerpt();
        $featured_img_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
        $author_id  = get_the_author_meta( 'ID' );
        $firstname  = get_the_author_meta( 'user_firstname', $author_id );
        $lastname   = get_the_author_meta( 'user_lastname', $author_id );
        $user_meta  = get_userdata($author_id);
        $user_roles = $user_meta->roles;
        ?>
        <div class="column large-5 large-offset-1 medium-12 small-12">
          <div class="text">
            <h2><?php the_title(); ?></h2>
            <p><?php echo $excerpt; ?></p>
            <?php
            echo $firstname. ' ' .$lastname;
            ?>
            <?php foreach($user_roles as $user_role ){
              $role = $user_role;
              if( $role == 'editor' ){
                $role = 'Redacteur';
              }
              echo $role;
            } ?>
          </div>
        </div>
        <div class="column large-5 medium-12 small-12">
          <div class="image-wrapper">
            <div class="image-container" style="background-image: url('<?php echo $featured_img_url; ?>');"></div>
          </div>
        </div>
        <?php
      }
      wp_reset_postdata(); ?>
      <?php
    }
    ?>
  </div>
</div>
<!-- end:<?php echo $prefix; ?> -->
