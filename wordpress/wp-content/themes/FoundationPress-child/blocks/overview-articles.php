<?php
$rowlayout        = basename( __FILE__, ".php" );
$prefix           = $rowlayout.'-';

$title            = get_sub_field( $prefix.'title' );
?>
<!-- <?php echo $prefix; ?> -->
<div class="section <?php echo $rowlayout; ?>">
  <div class="row">
    <div class="column large-7 medium-12 small-12">
      <?php
      $args = array(
      	'post_type'              => [ 'article' ],
      	'posts_per_page'         => '1',
      );
      $query = new WP_Query( $args );
      if ( $query->have_posts() ) {
      	while ( $query->have_posts() ) {
      		$query->the_post();
          get_template_part( 'blocks/partials/part', 'featured-article' );
      	}
      }
      wp_reset_postdata();
      ?>
    </div>
    <div class="column large-5 medium-12 small-12">
      <?php
      $args = array(
        'post_type'              => [ 'article' ],
        'posts_per_page'         => '6',
        'offset'                 => '1',
      );
      $query = new WP_Query( $args );
      if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
          $query->the_post();
          get_template_part( 'blocks/partials/part', 'article-list' );
        }
      }
      wp_reset_postdata();
      ?>
    </div>
  </div>
</div>
<!-- end:<?php echo $prefix; ?> -->
