<?php
$rowlayout        = basename( __FILE__, ".php" );
$prefix           = $rowlayout.'-';

$title            = get_sub_field( $prefix.'title' );
$terms            = get_terms( 'article_category' );
?>
<!-- <?php echo $prefix; ?> -->
<div class="section <?php echo $rowlayout; ?>">
  <div class="row">
    <div class="column large-7 medium-12 small-12">
      <h2>Browse door  thema’s</h2>
    </div>
    <div class="column large-8 medium-12 small-12">
      <div class="blog-terms">
        <ul>
          <?php
          $i = 1;
          foreach( $terms as $term ) {
            $i++;
          }
          if ( $i > 0 ) { ?>
          <li><span>Categorie:</span></li>
          <?php } ?>
          <?php
          $i = 1;
          foreach( $terms as $term ) {
            $name         = $term->name;
            $description  = $term->description;
            $termlink     = get_term_link($term); ?>
            <li class="<?php echo $i > 1 ? 'hidden' : ''; ?>">
              <i class="fas fa-tag"></i> <a target="_blank" class="link" href="<?php echo $termlink; ?>"> <?php echo $name; ?></a>
            </li>
            <?php
            $i++;
          }
          ?>
          <?php if ( $i > 2 || !$i ===  0 ) { ?>
            <li>
              <div class="btn-container"><span class="load-more">Meer + </span></div>
              <div class="btn-container"><span class="load-less">Minder - </span></div>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- end:<?php echo $prefix; ?> -->
