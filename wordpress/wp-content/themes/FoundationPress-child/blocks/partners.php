<?php
$rowlayout        = basename( __FILE__, ".php" );
$prefix           = $rowlayout.'-';

$title            = get_sub_field( $prefix.'title' );
$partners         = get_sub_field( $prefix.'partner-select' );
?>
<!-- <?php echo $prefix; ?> -->
<div class="section <?php echo $rowlayout; ?>">
  <div class="row">
    <div class="column large-12 medium-12 small-12">
      <?php
      if( $title ){
        ?>
        <h2><?php echo $title; ?></h2>
        <?php
      }
      ?>
      <div class="partner-slider-wrapper">
        <?php
        if( $partners ){
          ?>
          <?php
          foreach( $partners as $partner ){
            $logo         = get_the_post_thumbnail_url( $partner->ID , 'thumbnail' );
            $partnertitle = get_the_title( $partner->ID );
           ?>
            <div class="logo">
              <img src="<?php echo $logo; ?>" alt="<?php echo $partnertitle; ?>" />
            </div>
            <?php
          } ?>
          <?php
        } ?>
      </div>
    </div>
  </div>
</div>
<!-- end:<?php echo $prefix; ?> -->
